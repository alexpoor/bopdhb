# Web app for mapping of MHA services in BOP

## Background
This project builds a basic web app which allows MHA services within BOP DHB region to be mapped interactively.

The app has the following components:
* Leaflet.js - core library for mappig
* Leaflet.markercluster - allows clustering of markers on the map
* GeoJSON layer files for defining polygons (DHB regions or IMD areas)
* Other supplied data (eg. list of providers with relevant metadata).

## Usage
The layer files and supplied data is not tracked in this repo, but can easily be obtained.

With these in place, it's just a question of making the html file available on a server somewhere it can be accessed.

There is no bigger framework used - for example around user management, or embedding in other pages. But the map object in this file can easily be placed in a div or an iframe for use elsewhere.